import Foundation

func transcriptionsList() -> Array<Dictionary<String, Any>> {
    
    // un array cu 3 obiecte, fiecare dictionar sa aiba 4 chei
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "YYYY-MM-DD"
    
    let date1String = "2016-12-10"
    let date1 = dateFormatter.date(from: date1String)!
    
    let transcription1 = [
        "index" : 1,
        "text" : "Transcriere 1",
        "audio.mp4" : "http://transcription-example/audio1.mp4",
        "redactor" : "Ion Popescu",
        "data" : date1
        ] as [String : Any]
    
    let date2String = "2016-12-03"
    let date2 = dateFormatter.date(from: date2String)!
    
    let transcription2 = [
        "index" : 2,
        "text" : "Transcriere 2",
        "audio.mp4" : "http://transcription-example/audio2.mp4",
        "redactor" : "Ion Georgescu",
        "data" : date2
        ] as [String : Any]
    
    let date3String = "2016-11-26"
    let date3 = dateFormatter.date(from: date3String)!
    
    let transcription3 = [
        "index" : 3,
        "text" : "Transcriere 3",
        "audio.mp4" : "http://transcription-example/audio3.mp4",
        "redactor" : "Ion Popescu",
        "data" : date3
        ] as [String : Any]
    
    let transcriptions = [transcription1, transcription2, transcription3]
    
    return transcriptions
}
