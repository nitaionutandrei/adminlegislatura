import UIKit

class ViewController: UIViewController {
    
    override func viewDidAppear(_ animated: Bool) {
        let transcriptions = transcriptionsList()
        // print (transcriptions)
        
        let userDefaults = UserDefaults.standard
        let storedTranscriptions = userDefaults.value(forKey: "Transcriptions")
        
        if storedTranscriptions != nil {
            print (storedTranscriptions!)
        }else{
            userDefaults.set(transcriptions, forKey: "Transcriptions")
        }
        
    }
}
